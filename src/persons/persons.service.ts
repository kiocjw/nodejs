import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Person, PersonDocument } from './persons.schema';
import { PersonInput } from './inputs/person.input';

@Injectable()
export class PersonsService {
  constructor(
    @InjectModel(Person.name) private personModel: Model<PersonDocument>,
  ) {}

  async create(createPersonDto: PersonInput): Promise<Person> {
    const createdPerson = new this.personModel(createPersonDto);
    return createdPerson.save();
  }

  async createurl(createPersonDto: PersonInput): Promise<string> {
    const createdPerson = new this.personModel(createPersonDto);
    return (await createdPerson.save()).id;
  }

  async findByID(ID: string): Promise<Person> {
    return this.personModel.findById(ID).exec();
  }

  async findByPassport(pass: string): Promise<Person> {
    return this.personModel.findOne({ passport: pass }).exec();
  }

  async findOne(ID: string): Promise<Person | undefined> {
    return this.personModel.findById(ID).exec();
  }

  async findAll(): Promise<Person[]> {
    return this.personModel.find().exec();
  }
}
