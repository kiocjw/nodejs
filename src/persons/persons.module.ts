import { Module } from '@nestjs/common';
import { PersonsResolver } from './persons.resolver';
import { Person, PersonSchema } from './persons.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonsService } from './persons.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Person.name, schema: PersonSchema }]),
  ],
  providers: [PersonsResolver, PersonsService],
  exports: [PersonsService],
})
export class PersonsModule {}
